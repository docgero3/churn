import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import joblib
#import warnings
#warnings.filterwarnings("ignore")


#new Dataset 2019-2022

#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'
file = 'Refdata.xlsx'
sheetname = 'Sheet1'

#file = path+fichier
# DataFrame df:
dataset = pd.read_excel(file, sheetname)


t0= pd.merge(pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier','Solution.Solution Line'],values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).count()).to_records()).rename(columns = {"('sum', 'Amount in €')":"Frequency"}), pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier','Solution.Solution Line'],values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).sum()).to_records()).rename(columns = {"('sum', 'Amount in €')":"Monetary"}) ,on=["Carrier.Carrier", "Carrier.Carrier"])

# index
t1=pd.merge(pd.DataFrame(dataset.groupby(by=['Carrier.Carrier'],as_index=False)["Solution.Solution"].count().to_records()),t0,on=["Carrier.Carrier", "Carrier.Carrier"])
#merge



df1= pd.merge(t1,pd.merge(pd.DataFrame((dataset.groupby(by='Carrier.Carrier',as_index=False)["Time.Month"].min()).to_records()).rename(columns = {"Time.Month":"FirstPurchaseDate"}),pd.DataFrame((dataset.groupby(by='Carrier.Carrier',as_index=False)["Time.Month"].max()).to_records()).rename(columns = {"Time.Month":"LastPurchaseDate"}),on=["Carrier.Carrier", "Carrier.Carrier"]),on=["Carrier.Carrier", "Carrier.Carrier"])
df1["FirstPurchaseDate"]=df1["FirstPurchaseDate"].astype(str)+'01'
df1["LastPurchaseDate"]=df1["LastPurchaseDate"].astype(str)+'01'
df1["FirstPurchaseDate"] = pd.to_datetime(df1["FirstPurchaseDate"] , format='%Y/%m/%d', errors='coerce')
df1["LastPurchaseDate"] = pd.to_datetime(df1["LastPurchaseDate"] , format='%Y/%m/%d', errors='coerce')

#Lifetime and recency
today_date=pd.to_datetime("today")
df1['Lifetime']=  df1['LastPurchaseDate']- df1['FirstPurchaseDate'] 
df1['Lifetime']=round((df1['Lifetime']/ np.timedelta64(1, 'D')).astype(int)/30)

df1['Recency']= today_date - df1['LastPurchaseDate']
df1['Recency']=round((df1['Recency']/ np.timedelta64(1, 'D')).astype(int)/30)


data_rfm=df1.loc[:,['Recency', 'Frequency', 'Monetary','Lifetime']]
data_rfm["Monetary"] = data_rfm["Monetary"].fillna(0)

data_rfm = np.log(data_rfm+1)


scaler = StandardScaler()
scaler.fit(data_rfm)
RFM_Table_scaled = scaler.transform(data_rfm)

RFM_Table_scaled=pd.DataFrame(RFM_Table_scaled,columns=data_rfm.columns)
df2=df1.loc[:,["Carrier.Carrier","index"]]
data_rfm["Recency"]  = data_rfm["Recency"].fillna(0)
RFM_Table_scaled["Recency"]  = RFM_Table_scaled["Recency"].fillna(0)

churn=pd.concat([pd.DataFrame(df1,columns=df2.columns),RFM_Table_scaled],axis=1)
x = churn.drop(['Carrier.Carrier'], axis=1)
x['Monetary'] = x['Monetary'].replace(np.nan, 0)


#Load model RF
# load the model from disk
filename = 'RandomForest_model.sav'
model = joblib.load(filename)
#result = loaded_model.score(X_test, Y_test)
# load the model
#model = pickle.load(open("model.pkl", "rb"))

# use model to predict
y_pred = model.predict(x)

pred=pd.DataFrame(y_pred).rename(columns = {0:"Churn"})
df2=pd.concat([churn,pred],axis=1)

df2.info()
