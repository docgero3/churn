import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt

from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import RandomizedSearchCV
#from sklearn.preprocessing import LabelEncoder

from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier

#from datetime import datetime
#from scipy import stats
from sklearn.preprocessing import StandardScaler

from sklearn import metrics

#load ans save model
import joblib

from sklearn import model_selection
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score, RandomizedSearchCV
import warnings
warnings.filterwarnings("ignore")


#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'
file = 'Ref2016-2018.xlsx'
sheetname = 'ref'

#file = path+fichier

# DataFrame df:
dataset = pd.read_excel(file, sheetname)
dataset['Time.Date']=dataset['Time.Month'].astype(str)+'01'
dataset['Time.Date'] = pd.to_datetime(dataset['Time.Date'], format='%Y/%m/%d')
#dataset1=dataset

t0= pd.merge(pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier','Solution.Solution Line'],values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).count()).to_records()).rename(columns = {"('sum', 'Amount in €')":"Frequency"}), pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier','Solution.Solution Line'],values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).sum()).to_records()).rename(columns = {"('sum', 'Amount in €')":"Monetary"}) ,on=["Carrier.Carrier", "Carrier.Carrier"])

# index
t1=pd.merge(pd.DataFrame(dataset.groupby(by=['Carrier.Carrier'],as_index=False)["Solution.Solution"].count().to_records()),t0,on=["Carrier.Carrier", "Carrier.Carrier"])

#Frequency
t2=pd.merge(pd.merge(t1,pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier'],columns = 'Time.Year',values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).sum()).to_records()).rename(columns = {"('sum', 'Amount in €', 2018)":"2018","('sum', 'Amount in €', 2017)":"2017","('sum', 'Amount in €', 2016)":"2016","('sum', 'Amount in €', 2021)":"2021"}),on=["Carrier.Carrier", "Carrier.Carrier"]) ,pd.DataFrame(dataset.pivot_table(index=['Carrier.Carrier'], columns = 'Solution.Solution Line', values='Amount in €',aggfunc=['sum']).to_records()).rename(columns = {"('sum', 'BANDWIDTH')":"BW","('sum', 'IP')":"IP","('sum', 'MESSAGING')":"MESSAGING","('sum', 'MOBILE')":"MOBILE","('sum', 'NCP')":"NCP","('sum', 'ROAMING SERVICES')":"ROAMING SERVICES","('sum', 'VOICE')":"VOICE"}),on=["Carrier.Carrier", "Carrier.Carrier"])

t3=pd.merge(pd.DataFrame((dataset.pivot_table(index=['Carrier.Carrier'],columns = 'Time.Month',values=['Amount in €'],aggfunc=['sum'])
.groupby(['Carrier.Carrier']).sum()).to_records()).rename(columns = {"('sum', 'Amount in €', 201806)":"201806","('sum', 'Amount in €', 201807)":"201807","('sum', 'Amount in €', 201808)":"201808","('sum', 'Amount in €', 201809)":"201809","('sum', 'Amount in €', 201809)":"201809","('sum', 'Amount in €', 201810)":"201810","('sum', 'Amount in €', 201811)":"201811","('sum', 'Amount in €', 201812)":"201812"}) ,pd.DataFrame(dataset.pivot_table(index=['Carrier.Carrier'], columns = 'Solution.Solution Line', values='Amount in €',aggfunc=['sum']).to_records()).rename(columns = {"('sum', 'BANDWIDTH')":"BW","('sum', 'IP')":"IP","('sum', 'MESSAGING')":"MESSAGING","('sum', 'MOBILE')":"MOBILE","('sum', 'NCP')":"NCP","('sum', 'ROAMING SERVICES')":"ROAMING SERVICES","('sum', 'VOICE')":"VOICE"}),on=["Carrier.Carrier", "Carrier.Carrier"])
t=pd.merge(t2,t3,on=["Carrier.Carrier", "Carrier.Carrier"])

#merge

dataset= pd.merge(t, pd.merge(pd.DataFrame((dataset.groupby(by='Carrier.Carrier',as_index=False)["Time.Month"].min()).to_records()).rename(columns = {"Time.Month":"FirstPurchaseDate"}),pd.DataFrame((dataset.groupby(by='Carrier.Carrier',as_index=False)["Time.Month"].max()).to_records()).rename(columns = {"Time.Month":"LastPurchaseDate"}),on=["Carrier.Carrier", "Carrier.Carrier"])
,on=["Carrier.Carrier", "Carrier.Carrier"])
dataset["FirstPurchaseDate"]=dataset["FirstPurchaseDate"].astype(str)+'01'
dataset["LastPurchaseDate"]=dataset["LastPurchaseDate"].astype(str)+'01'
dataset["FirstPurchaseDate"] = pd.to_datetime(dataset["FirstPurchaseDate"] , format='%Y/%m/%d', errors='coerce')
dataset["LastPurchaseDate"] = pd.to_datetime(dataset["LastPurchaseDate"] , format='%Y/%m/%d', errors='coerce')

#Lifetime and recency
today_date=pd.to_datetime("today")
dataset['Lifetime']=  dataset['LastPurchaseDate']- dataset['FirstPurchaseDate'] 
dataset['Lifetime']=round((dataset['Lifetime']/ np.timedelta64(1, 'D')).astype(int)/30)

dataset['Recency']= today_date - dataset['LastPurchaseDate']
dataset['Recency']=round((dataset['Recency']/ np.timedelta64(1, 'D')).astype(int)/30)

#Monetary
data_rfm=dataset.loc[:,['Recency', 'Frequency', 'Monetary','Lifetime']]
data_rfm["Monetary"] = data_rfm["Monetary"].fillna(0)
data_rfm['Monetary'] = data_rfm['Monetary'].replace(np.nan, 0)





# Scaling

data_rfm = np.log(data_rfm+1)
#Removing Skewness
scaler = StandardScaler()
scaler.fit(data_rfm)
RFM_Table_scaled = scaler.transform(data_rfm)

RFM_Table_scaled=pd.DataFrame(RFM_Table_scaled,columns=data_rfm.columns)

data=pd.concat([dataset[["Carrier.Carrier","index","201806","201807","201808","201809","201810","201811","201812"]],RFM_Table_scaled],axis=1)


data.loc[(data["201812"] == 0) | (data["201811"] == 0) | (data["201810"] == 0) | (data["201809"] == 0) | (data["201808"] == 0) | (data["201807"] == 0) | (data["201806"] == 0),'Churn']=1
data.loc[(data["201812"] != 0) | (data["201811"] != 0) | (data["201810"] != 0) | (data["201809"] != 0) | (data["201808"] != 0) | (data["201807"] != 0) | (data["201806"] != 0),'Churn']=0

#Data split
def clean_dataset(df):
    assert isinstance(df, pd.DataFrame), "df needs to be a pd.DataFrame"
    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    return df[indices_to_keep].astype(np.float64)

data1=data.drop(['Carrier.Carrier'], axis=1)
clean_dataset(data1)
data2=data1[["index","Recency","Frequency", "Monetary","Lifetime","Churn"]]

y = pd.DataFrame(data2['Churn'])
print("Churn for the dataset 2016-2018")
print(f'Percentage of Churn:  {round(y.value_counts(normalize=True)[1]*100,2)} %  --> ({y.value_counts()[1]} customers)')
print(f'Percentage Non_Churn: {round(y.value_counts(normalize=True)[0]*100,2)}  %  --> ({y.value_counts()[0]} customers)')

x = data2.drop(['Churn'], axis=1)

#Split dataset to train 70% and test 30%
X_train, X_test, y_train, y_test = train_test_split(x,y, test_size=0.3, random_state=42)

## Stochastic Gradient Decent Classifier
print("Stochastic Gradient Decent Classifier")
sgd = SGDClassifier(penalty=None)
sgd.fit(X_train, y_train)
pred_sgd = sgd.predict(X_test)
pred_sgd_train = sgd.predict(X_train)

#Let's see how our model performed 
print("classification_report train")
print(classification_report(y_train, pred_sgd_train))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_train, pred_sgd_train), 3))

#Let's see how our model performed
print("classification_report test")
print(classification_report(y_test, pred_sgd))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_test, pred_sgd), 3))


##Logistic regression
print("Logistic regression")
from sklearn.linear_model import LogisticRegression 
logreg=LogisticRegression()
logreg.fit(X_train,y_train.values.ravel())
prediction_logreg=logreg.predict(X_test)
#print(accuracy_score(y_test,prediction_logreg))
prediction_logreg_train = logreg.predict(X_train)


#Let's see how our model performed 
print("classification_report train")
print(classification_report(y_train, prediction_logreg_train))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_train, prediction_logreg_train), 3))

#Let's see how our model performed
print("classification_report test")
print(classification_report(y_test, prediction_logreg))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_test, prediction_logreg), 3))


## random Forest
print("Random Forest")
from sklearn.ensemble import RandomForestClassifier
rfc = RandomForestClassifier()
rfc.fit(X_train, y_train.values.ravel())
pred_rfc = rfc.predict(X_test)
pred_train = rfc.predict(X_train)

#Let's see how our model performed 
print("classification_report train")
print(classification_report(y_train, pred_train))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_train, pred_train), 3))

#Let's see how our model performed
print("classification_report test")
print(classification_report(y_test, pred_rfc))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_test, pred_rfc), 3))
rfc_defaut=round(metrics.accuracy_score(y_test, pred_rfc), 3)


## decision Tree
print("Decision Tree")
from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier()
dt.fit(X_train,y_train.values.ravel())
pred_dt = dt.predict(X_test)
preddt_train = dt.predict(X_train)

#Let's see how our model performed 
print("classification_report train")
print(classification_report(y_train, preddt_train))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_train, preddt_train), 3))

#Let's see how our model performed
print("classification_report validation")
print(classification_report(y_test, pred_dt))
print ("Overall Accuracy:", round(metrics.accuracy_score(y_test, pred_dt), 3))

## Model Random Forest

X = x
Y = y


test_size = 0.3
seed = 7
X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, test_size=test_size, random_state=seed)
# Fit the model on training set
model = RandomForestClassifier()
model.fit(X_train, Y_train.values.ravel())

# Fine-tuning
print ("Fine-tuning")
# Designate distributions to sample hyperparameters from
n_estimators = np.random.uniform(70, 90, 8).astype(int)
max_features = np.random.normal(8, 2, 8).astype(int)

# Check max_features>0 & max_features<=total number of features
max_features[max_features <= 0] = 1
max_features[max_features > X.shape[1]] = X.shape[1]

hyperparameters = {'n_estimators': list(n_estimators),
                   'max_features': list(max_features)}
print("Random Forest model")
print (hyperparameters)



# Run randomized search
randomCV = RandomizedSearchCV(RandomForestClassifier(), param_distributions=hyperparameters, n_iter=20)
randomCV.fit(X_train, y_train)

# Identify optimal hyperparameter values
best_n_estim      = randomCV.best_params_['n_estimators']
best_max_features = randomCV.best_params_['max_features']

print("The best performing n_estimators value is: {:5d}".format(best_n_estim))
print("The best performing max_features value is: {:5d}".format(best_max_features))


# Train classifier using optimal hyperparameter values
# We could have also gotten this model out from randomCV.best_estimator_
rfc2 = RandomForestClassifier(n_estimators=best_n_estim,
                            max_features=best_max_features)

rfc2.fit(X_train, y_train)
rfc2_predictions = rfc2.predict(X_test)

print (metrics.classification_report(y_test, rfc2_predictions))

print ("Overall Accuracy optimal:", round(metrics.accuracy_score(y_test, rfc2_predictions), 3))
print("Overall Accuracy par defaut :",rfc_defaut)

#Save Model

print("Save the model to disk")

# save the model to disk
filename ='RandomForest_model.sav'
joblib.dump(model, filename)
 
# some time later...
print("some time later...")

#Load Model
print("Load Random Forest model")
# load the model from disk
loaded_model = joblib.load(filename)
result = loaded_model.score(X_test, Y_test)
print(result)

